"""_____________________________________________________________________

:PROJECT: LARA

*lara_devices models *

:details: lara_devices database models. 
          generic data model for storing
          parts, device classes (types of devices) 
          and concrete entities of devices.

:file:    models.py
:authors: mark doerr

:date: (creation)          20180625
:date: (last modification) 20190125

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.3"

        
import logging
from datetime import date, datetime, timedelta

from django.utils import timezone

from django.contrib.auth.models import Group
from django.db import models
from lara_people.models import Entity, Address

class ExtraDataType(models.Model):
    """EntityExtendedDataType: 
       e.g. email_home, email_lab, telephone_number_home1, company_x_customer_number, 
    """
    def __init__ (self):
        """ Class initialiser """
        extra_type_id =  models.AutoField(primary_key=True)
        description = models.TextField(blank=True, null=True)
        
    class Meta:
        db_table = "lara_parts_extra_data_type"
        
class ExtraData(models.Model):
    """This class can be used to extend the Entity data, by extra information, 
       e.g. more telephone numbers, customer numbers, ... """
    def __init__ (self):
        """ Class initialiser """
        extra_id =  models.AutoField(primary_key=True)
        data_type = models.ForeignKey(ExtraDataType, related_name="%(app_label)s_%(class)s_extra_data_related", 
                            related_query_name="%(app_label)s_%(class)s_extra_data",on_delete=models.CASCADE, blank=True)
        extra_text = models.TextField(blank=True, null=True) # generic text field
        extra_bin = models.BinaryField(blank=True, null=True) # generic binary field
        extra_file = models.FileField(upload_to='lara_devices/', blank=True, null=True) # rel. path/filename 
        
    class Meta:
        db_table = "lara_parts_extra_data"
        
class PartDeviceClass(models.Model):
    """ classes for parts and device, e.g. screw, bolt, robot, spectrometer, HPLC, NMR,  """
    class_id = models.AutoField(primary_key=True)
    name = models.TextField(unique=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name or ''
        
    class Meta:
        db_table = "lara_devices_part_device_class"

# Storing the device state in the LARA database needs to be discussed        
#~ class PartDeviceState(models.Model):
    #~ """ device states, e.g. init, idle, busy, error, warning, online, waiting, shutting down, off """
    #~ state_id = models.AutoField(primary_key=True)
    #~ state = models.TextField(unique=True)
    #~ description = models.TextField(blank=True)
    #~ def __str__(self):
        #~ return self.state or ''
    #~ class Meta:
        #~ db_table = "lara_devices_device_state"

class PartAbstr(models.Model):
    """
    A Part is an artifact that is used to build a device or perform an experiment (screw, nut, bolt, transistor, IC, lamp, diode, ... ).
    """
    name = models.TextField(unique=True)
    full_name = models.TextField(blank=True)
    slug = models.SlugField(max_length=255, blank=True) # should be unique slug name without special characters ?? - unique generation does not work with fixtures !
    fabricator = models.ForeignKey('lara_people.Entity', on_delete=models.CASCADE, blank=True, null=True)
    model_no = models.TextField(blank=True) # = part number
    serial_no = models.TextField(blank=True)
    service_no = models.TextField(blank=True)
    registration_no = models.TextField(blank=True) # can be used for internal registrations
    fabrication_date = models.DateField(default=date(1,1,1))
    purchase_date = models.DateField(default=date(1,1,1)) # should be moved to ordering
    end_of_warrenty_date = models.DateField(default="0001-01-01")
    #~ state = models.ForeignKey(PartDeviceState, on_delete=models.CASCADE, blank=True, null=True) # online, waiting, busy, shutting down, off; default: init
    operator_group = models.ForeignKey(Group,related_name='%(app_label)s_%(class)s_operator_group_related',
                related_query_name="%(app_label)s_%(class)s_operator_group", on_delete=models.CASCADE, blank=True, null=True)
    service_group = models.ForeignKey(Group, related_name='%(app_label)s_%(class)s_service_group_related', 
                related_query_name="%(app_label)s_%(class)s_service_group", on_delete=models.CASCADE, blank=True, null=True)
    description = models.TextField(blank=True)
    extra_data = models.ManyToManyField(ExtraData, related_name='%(app_label)s_%(class)s_extradata_related', 
                related_query_name="%(app_label)s_%(class)s_extradata", blank=True) # specifications, maintanance plan, maintanance history, incidents history, network settings, errors, warnings, SOPs, image filename
    image = models.ImageField(upload_to='lara_devices/', blank=True, default="") # rel. path/filename to image
    # ',UNIQUE(barcode) ON CONFLICT IGNORE )'),
    
    def warrentyLeft(self, day_interval = 7):
        ''' choose day_interval = 7 for weeks
        '''
        now = datetime.now()
        warrenty_end = datetime.combine( self.end_of_warrenty_date, datetime.min.time()) # datetime object is required for substraction
        warrenty_left = warrenty_end - now 
        
        return warrenty_left.days / day_interval 
        
    warrentyLeft.admin_order_field = 'end_of_warrenty_date'
    warrentyLeft.short_description = "Warrenty left [weeks]"
         
    def endOfWarrentySoon(self):
        now = datetime.now()
        warrenty_end = datetime.combine( self.end_of_warrenty_date, datetime.min.time())
        warrenty_left =  warrenty_end - now
        
        return warrenty_left < timedelta(weeks=12)
        
    endOfWarrentySoon.admin_order_field = 'end_of_warrenty_date'
    endOfWarrentySoon.boolean = True
    endOfWarrentySoon.short_description = 'Warrenty ends soon ?'
        
    def inWarrentyPeriode(self):
        today = date.today()

        return( self.end_of_warrenty_date > today )
        
    inWarrentyPeriode.admin_order_field = 'end_of_warrenty_date'
    inWarrentyPeriode.boolean = True
    inWarrentyPeriode.short_description = 'Still Warrenty ?'
    
    def save(self, force_insert=None, using=None):
        ''' Here we generate some default values for full_name '''
        if not self.full_name:
            self.name = u' '.join((self.name, self.model_no, serial_no))
                       
        if not self.name_slug:
            self.name_slug = '-'.join((self.name, self.model_no, serial_no )).lower()
            
        #~ if not self.image_filename:
            #~ self.image_filename = u'-'.join((self.name, self.model_no, '.png')).lower()
            
        super(PartAbstr, self).save(force_insert=force_insert, using=using)

    def __str__(self):
        return self.name or ''
    
    class Meta:
        abstract = True

class Part(PartAbstr):
    part_id = models.AutoField(primary_key=True)
    part_class = models.ForeignKey(PartDeviceClass, on_delete=models.CASCADE, blank=True, null=True) # screw, bolt, nut, lamp, diode, resitor, IC, ...
        
class Device(PartAbstr):
    """ generic device class - should be extended to parts """
    device_id = models.AutoField(primary_key=True)
    device_class = models.ForeignKey(PartDeviceClass,on_delete=models.CASCADE, blank=True, null=True) # UV-spectrometer, MALDI-TOF-MS, HPLC, ..
    
    # booking should be done from Calendar side
    #~ booking_events = models.ManyToManyField('CalendarEvent', related_name='device_booking_event', blank=True)
    #~ maintenance_events = models.ManyToManyField('CalendarEvent', related_name='device_maintenace_event', blank=True)
    # device state
    # maintainer group
   
class DeviceGroup(models.Model):
    """ device group for combining/pooling devices
        e.g. robotic platform with many devices
    """
    device_group_id =  models.AutoField(primary_key=True)
    name = models.TextField(unique=True) # group name
    members = models.ManyToManyField('Device', related_name='device_group_members', blank=True)
    colour = models.TextField(blank=True) # colour for better visual representation, e.g. in calendars ... 
    #~ metainfo = models.ManyToManyField('lara_metainfo.MetaInfo', related_name='%(app_label)s_%(class)s_metainfo_related', 
        #~ related_query_name="%(app_label)s_%(class)s_metainfo", blank=True) # specifications, maintanance plan, maintanance history, incidents history, network settings, SOPs, image filename
    description = models.TextField(blank=True)
    
    def __str__(self):
        return(self.name) or ''
        
    class Meta:
        db_table = "lara_devices_device_group"

# this should move to LARA-django-calendar
#~ class CalendarEvent(models.Model):
    #~ """The event set a record for an 
    #~ activity that will be scheduled at a 
    #~ specified date and time. 
    
    #~ types could be organised on event level or extra calendar layer (with different calendars for booking and maintainance)
    
    #~ It could be on a date and time 
    #~ to start and end, but can also be all day.
    
    #~ :param title: Title of event
    #~ :type title: str.
    
    #~ :param start: Start date of event
    #~ :type start: datetime.
    
    #~ :param end: End date of event
    #~ :type end: datetime.
    
    #~ :param all_day: Define event for all day
    #~ :type all_day: bool.
    
    #~ .. todo:: - types: booking, maintainance 
              #~ - user who booked / maintained
              #~ - JSON for direct storage
    #~ """
    #~ cal_event_id = models.AutoField(primary_key=True)
    #~ title = models.CharField(blank=True, max_length=200)
    #~ start = models.DateTimeField()
    #~ end = models.DateTimeField()
    #~ all_day = models.BooleanField(default=False)
    #~ ##~ user = operator_group = models.ForeignKey('lara_people.LaraUser',related_name='%(app_label)s_%(class)s_calendar_user_related',
     #~ #           #~ related_query_name="%(app_label)s_%(class)s_calendar_user", on_delete=models.CASCADE, blank=True, null=True)
    #~ JSON = models.TextField(blank=True)
        
    #~ def __str__(self):
        #~ return self.title or ''

