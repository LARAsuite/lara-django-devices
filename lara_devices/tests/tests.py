"""_____________________________________________________________________

:PROJECT: LARA

*lara_devices tests *

:details: lara_devices application tests.
         - 

:file:    tests.py
:authors: mark doerr

:date: (creation)          20180627
:date: (last modification) 20180627

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

from django.test import TestCase
import logging

logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
#~ logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

# Create your tests here. s. https://docs.djangoproject.com/en/1.9/intro/tutorial05/

import datetime

from django.utils import timezone
from django.test import TestCase

#~ from django.core.urlresolvers import reverse # needs to be changed in dango 1.10 to:
from django.urls import reverse

from django.contrib.auth.models import User

from ..models import Device

def createDevice(name='test_dev', delta_weeks=14):
    """ Creates a Device entry
    """

    #state 
    #responsible_group  
    
    end_of_warrenty_date = datetime.date.today() +  datetime.timedelta(weeks=delta_weeks)
    
    return Device.objects.create(name=name,
                                full_name='test device',
                                model_no='',
                                serial_no='',
                                service_no='',
                                registration_no='',
                                fabrication_date=datetime.date.today(),
                                purchase_date=datetime.date.today(),
                                end_of_warrenty_date=end_of_warrenty_date )

class DevicesMethodsTests(TestCase):
    def testDeviceCreation(self):
        """ Testing  
        """
        curr_dev = createDevice()
        
        self.assertEqual(curr_dev.name, "test_dev" )
        
    def testWarrentyLeft(self):
        """ Testing  
        """
        curr_dev = createDevice(name="dev1",delta_weeks=1)
        
        self.assertEqual(curr_dev.warrentyLeft(), 0  )
        
        curr_dev = createDevice(name="dev2", delta_weeks=14)

        self.assertEqual(curr_dev.warrentyLeft(), 13  )

    def testEndOfWarrentySoon(self):
        """ Testing
        """
        curr_dev = createDevice(name="dev1", delta_weeks=14)
        
        self.assertEqual(curr_dev.endOfWarrentySoon(), False )
        
        curr_dev = createDevice(name="dev2", delta_weeks=2)
        
        self.assertEqual(curr_dev.endOfWarrentySoon(), True )
        
        
    def testInWarrentyPeriode(self):
        """ Testing  
        """
        curr_dev = createDevice(name="dev1", delta_weeks=14)
        
        self.assertEqual(curr_dev.inWarrentyPeriode(), True )

        curr_dev = createDevice(name="dev2", delta_weeks=0)
        
        self.assertEqual(curr_dev.inWarrentyPeriode(), False )
        
class DevicesViewsTests(TestCase):
    def testDeviceView(self):
        """ Testing device view
        """
        curr_dev = createDevice()
        
        response = self.client.get(reverse('lara_devices:index'))
        
        self.assertContains(response, "test_dev")
        
    def testSearchFormView(self):
        """ Testing search  view
        """
        curr_dev = createDevice()
        
        response = self.client.get(reverse('lara_devices:searchform'))
        
        self.assertContains(response, "Search")
