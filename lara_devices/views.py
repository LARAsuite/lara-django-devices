"""_____________________________________________________________________

:PROJECT: LARA

*lara_devices views *

:details: lara_devices views module.
         - 

:file:    views.py
:authors: mark doerr

:date: (creation)          20180627
:date: (last modification) 20180630

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import ListView
from django.views.generic import DetailView
from django.views.generic import View
from django.urls import reverse

from .models import Device #, CalendarEvent

import time
import datetime

#~ import sys

class IndexView(ListView) :
    model =  Device
    template_name = 'lara_devices/index.html' # otherwise it will look for device_list.html

class DeviceDetails(DetailView) :
    """Displays details to a selected device"""
    model = Device
    
class DeviceInfo(ListView) :
    """ Displays technical information about the devices, like serial numbers, fabricator, ..."""
    model =  Device
    #~ template_name = 'lara_devices/index.html'
    
#~ class DevicesListView(ListView) :
    #~ model = Device
    #~ template_name = 'lara_devices/device_list.html'
    
    #~ table_caption = "List of all LARA devices"
    #~ context = {'table_caption': table_caption}

#~ def searchForm(request):
    #~ return render(request, 'lara_devices/search.html', context={} )

class DeviceCalendar_disa(DetailView) :
    model = Device #CalendarEvent
    
    #~ def get_object(self, queryset=None):
        #~ curr_obj = super().get_object(queryset=queryset)
        #~ return curr_obj
            
    template_name = 'lara_devices/device_calendar_detail.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        event_list = ""
        
        calendar_options = """{{ defaultView: 'agendaWeek',
                        timeFormat: "HH:mm",
                        dateFormat: "Y/m/d",
                        header: {{
                            left: 'prev,next today',
                            center: 'title',
                            right: 'agendaWeek,agendaDay,month,',
                        }},
                        allDaySlot: false,
                        firstDay: 0,
                        weekMode: 'liquid',
                        slotMinutes: 15,
                        defaultEventMinutes: 30,
                        minTime: 8,
                        maxTime: 20,
                        editable: true,
                        eventLimit: true, // allow "more" link when too many events
                        events: [{calendar_events}],
                        dayClick: function(date, allDay, jsEvent, view) {{
                            if (allDay) {{       
                                $('#calendar').fullCalendar('gotoDate', date)      
                                $('#calendar').fullCalendar('changeView', 'agendaDay')
                            }}
                        }},
                        eventClick: function(event, jsEvent, view) {{
                            if (view.name == 'month') {{    
                                $('#calendar').fullCalendar('gotoDate', event.start)      
                                $('#calendar').fullCalendar('changeView', 'agendaDay')
                            }}
                        }},
                    }}"""
                    
        curr_device = self.object
    
        booking_events_qs = curr_device.booking_events.all()
        
        if booking_events_qs:
            event_list = ",".join([ bk_event.JSON for bk_event in booking_events_qs])
        
        #~ sys.stderr.write("ev_list =:{}\n".format( event_list ))
        
        # !! use the safe tag in the template to render this string !!!
        context['calendar_options'] = calendar_options.strip().format(calendar_events=event_list)
        return context

# ------- proof of concept stuff -------

def currentDatetime(request):
    now = datetime.datetime.now()
    html = "<html> <meta http-equiv=\"refresh\" content=\"5\" /> <body>It is now %s.</body></html>" % now 
    return HttpResponse(html)
    
class AjaxView(View):
    def get(self, request, **kwargs):
        return HttpResponse('hello AJAX world !')
