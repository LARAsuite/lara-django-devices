"""_____________________________________________________________________

:PROJECT: LARA

*lara_devices urls *

:details: lara_devices urls module.
         - add app specific urls here

:file:    urls.py
:authors: mark doerr

:date: (creation)          20180627
:date: (last modification) 20190125

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.2"

from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django.urls import reverse

from django.urls import path, include

from . import views

app_name = 'lara_devices' # !! this sets the apps namespace to be used in the template

# the 'name' attribute is used in templates to address the url independant of the view
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    #~ path('<int:pk>/', views.DeviceDetails.as_view(), name='device-detail'),
    path('device-info/', views.DeviceInfo.as_view(), name='device-info' ),
    #~ path('calendar/<int:pk>/', views.DeviceCalendar.as_view(), name='device-calendar' ),
    path('time/', views.currentDatetime ),
    #~ path('ajax/', views.AjaxView.as_view(), name='ajax-view'),
    #~ url(r'^$', views.IndexView.as_view(), name='index'),   # the 'name' value as called by the {% url %} template tag
    #~ url(r'^devicelist$', views.DevicesListView.as_view(), name='devices-list'),
    #~ url(r'^(?P<pk>[0-9]+)/$', views.DeviceDetails.as_view(), name='device-detail'),
    #~ path(r'zeroconf/', views.zeroconfBrowser ),
    #~ url(r'^search$', views.searchForm, name='searchform'),
    #~ url(r'^search-results$', views.search, name='search'),
    #~ url(r'^results$', views.results, name='results'),
    #~ url(r'^(?P<question_id>[0-9]+)/$', views.detail, name='detail'),
    #~ url(r'^(?P<question_id>[0-9]+)/results/$', views.results, name='results'),
    #~ url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#~ urlpatterns += patterns('lara_devices.views', url(r'^ajax/$', 'ajax_view', name='ajax-view'), )

def javascript_settings():
    js_conf = { 
                'ajax_view': reverse('lara_devices:ajax-view'),
              }
    return js_conf
