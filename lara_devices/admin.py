"""_____________________________________________________________________

:PROJECT: LARA

*lara_devices admin *

:details: lara_devices admin module admin backend configuration.
         - 

:file:    admin.py
:authors: mark doerr

:date: (creation)          20180627
:date: (last modification) 20190125

.. note:: - comp. new dev app in projects
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

from django.contrib import admin

from .models import PartDeviceClass, Part, Device

class DeviceClassAdmin(admin.ModelAdmin):
    list_display = ('name','description')
    list_filter = ['name']
    search_fields = ['name']
    
#~ class DeviceInline(admin.TabularInline):
    #~ model = Lara_group
    
class PartAdmin(admin.ModelAdmin):
    #~ fieldsets = [
        #~ (None,               {'fields': ['question_text']}),
        #~ ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    #~ ]
    #~ inlines = [DeviceInline]
     
    list_display = ('name', 'full_name', 'serial_no', 'fabrication_date', 'purchase_date', 'end_of_warrenty_date', 'warrentyLeft', 'inWarrentyPeriode' )
    list_filter = ['part_class']
    search_fields = ['name','full_name']
        
class DeviceAdmin(admin.ModelAdmin):
    #~ fieldsets = [
        #~ (None,               {'fields': ['question_text']}),
        #~ ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    #~ ]
    #~ inlines = [DeviceInline]
     
    list_display = ('name', 'full_name', 'serial_no', 'fabrication_date', 'purchase_date', 'end_of_warrenty_date', 'warrentyLeft', 'inWarrentyPeriode' )
    list_filter = ['device_class']
    search_fields = ['name','full_name']
    
#~ class DeviceBookingCalendarAdmin(admin.ModelAdmin):
    #~ list_display = ('title','start', 'end', 'all_day', 'JSON')
    #~ list_filter = ['title']
    #~ search_fields = ['title', 'start']
    
admin.site.register(PartDeviceClass, DeviceClassAdmin)
admin.site.register(Part, PartAdmin)
admin.site.register(Device, DeviceAdmin)
#~ admin.site.register(CalendarEvent, DeviceBookingCalendarAdmin)

