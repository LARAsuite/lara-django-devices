"""_____________________________________________________________________

:PROJECT: LARA

*lara_devices setup *

:details: lara_devices setup file for installation.
         - For installation, run:
           run pip3 install .
           or  python3 setup.py install

:file:    setup.py
:authors: mark doerr (mark.doerr@uni-greifswald.de)

:date: (creation)          
:date: (last modification) 20190127

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.3"

import os
import sys

from setuptools import setup, find_packages
#~ from distutils.sysconfig import get_python_lib

pkg_name = 'lara_devices'

def read(fname):
    try:
        return open(os.path.join(os.path.dirname(__file__), fname)).read()
    except IOError:
        return ''

install_requires = [] 
data_files = []
package_data = {pkg_name: ['fixtures/*.json', ]}
    
setup(name=pkg_name,
    version=__version__,
    description='lara_devices',
    long_description=read('README.rst'),
    author='',
    author_email='',
    keywords='lab automation, experiments, database, evaluation, visualisation, SiLA2, robots',
    url='https://github.com/LARAsuite/lara-django-app-template',
    license='GPL',
    packages=find_packages(), #['lara_devices'],
    #~ package_dir={'lara_devices':'lara_devices'},
    install_requires = install_requires,
    test_suite='',
    classifiers=['Development Status :: 1 - Development',
                   'Environment :: commandline',
                   'Framework :: gRPC',
                   'Intended Audience :: Developers',
                   'License :: OSI Approved :: GPL',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                   'Programming Language :: Python :: 3.5',
                   'Programming Language :: Python :: 3.6',
                   'Topic :: Utilities'],
    include_package_data=True,
    package_data = package_data,
    data_files=data_files,
)
